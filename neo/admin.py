from django.contrib import admin
from .models import Choice, Question
# Register your models here.
class ChoiceAdmin(admin.TabularInline):
    model = Choice
    extra = 2

class QuestionAdmin(admin.ModelAdmin):
    list_display = ['question_text', 'pub_date', 'was_published_recently']
    list_display_links = ['question_text']
    list_editable = ['pub_date']
    list_filter = ['pub_date', 'question_text']

    fieldsets = [
        (None, {'fields': ['question_text']}),
        ('Date information', {'fields': ['pub_date']}),
    ]

    inlines = [ChoiceAdmin]

admin.site.register(Question, QuestionAdmin)
