import datetime

from django.test import TestCase
from django.utils import timezone
from django.urls import reverse

from neo.models import Question

def createQuestion(question_text: str, days):
    time = timezone.now() + datetime.timedelta(days=days)
    Question.objects.create(question_text=question_text, pub_date=time)

class QuestionIndexViewTest(TestCase):
    def test_no_questions(self):
        response = self.client.get(reverse('neo:index'))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['last_questions'], [])

    def test_two_past_questions(self):
        createQuestion(question_text='First question?', days=-30)
        createQuestion(question_text='Second question?', days=-5)
        response = self.client.get(reverse('neo:index'))
        self.assertQuerysetEqual(
            response.context['last_questions'],
            ['<Question: Second question?>', '<Question: First question?>'])

    def test_future_question(self):
        createQuestion(question_text='future question?', days=5)
        response = self.client.get(reverse('neo:index'))
        self.assertQuerysetEqual(
            response.context['last_questions'], []
        )

    def test_future_and_past_question(self):
        createQuestion(question_text='Past question?', days=-5)
        createQuestion(question_text='Future question?', days=5)
        response = self.client.get(reverse('neo:index'))
        self.assertQuerysetEqual(
            response.context['last_questions'],
            ['<Question: Past question?>']
        )

class QuestionModelTests(TestCase):
    def test_was_published_recently_with_future_question(self):
        """
        was_published_recently() returns False for questions whose pub_date
        is in the future
        """
        question_date = timezone.now() + datetime.timedelta(days=30)
        future_question = Question(pub_date=question_date)
        self.assertFalse(future_question.was_published_recently())
    
    def test_was_published_recently_with_old_question(self):
        """
        was_published_recently() returns False for questions whose pub_date
        is older than 1 day
        """
        question_date = timezone.now() - datetime.timedelta(days=1)
        old_question = Question(pub_date=question_date)
        self.assertFalse(old_question.was_published_recently())

    def test_was_published_recently_with_current_question(self):
        """
        was_published_recently() returns True with questions whose pub_date
        is within the last day
        """
        question_date = timezone.now() - datetime.timedelta(
            hours=23, minutes=59, seconds=59)
        new_question = Question(pub_date=question_date)
        self.assertTrue(new_question.was_published_recently())
