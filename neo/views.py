from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils import timezone

from .models import Choice, Question

# Create your views here.
@login_required
def index(request):
    questions = Question.objects.filter(
        pub_date__lte=timezone.now()).order_by('-pub_date')[:5]
    context = {
        'last_questions': questions,
    }
    return render(request, 'neo/home.html', context)

@login_required
def detail(request, question_id):
    question = Question.objects.get(pk=question_id)
    return render(request, 'neo/detail.html', {'question': question})

@login_required
def results(request, question_id):
    question = Question.objects.get(pk=question_id)
    return render(request, 'neo/results.html', {'question': question})

def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        choice_selected = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'neo/detail.html', {
            'question': question,
            'error_message': 'You didn\'t select a choice',
        })
    else:
        choice_selected.votes += 1
        choice_selected.save()
        
        return HttpResponseRedirect(reverse('neo:results', args=(question_id,)))
